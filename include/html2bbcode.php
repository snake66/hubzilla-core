<?php /** @file */
/*
html2bbcode.php
Converter for HTML to BBCode
Made by: Mike@piratenpartei.de
Originally made for the syncom project: http://wiki.piratenpartei.de/Syncom
					https://github.com/annando/Syncom
*/

function node2bbcode(&$doc, $oldnode, $attributes, $startbb, $endbb)
{
	do {
		if (empty($startbb) && empty($endbb)) {
			break;
		}

		$done = node2bbcodesub($doc, $oldnode, $attributes, $startbb, $endbb);
	} while ($done);
}

function node2bbcodesub(&$doc, $oldnode, $attributes, $startbb, $endbb)
{
	$savestart = str_replace('$', '\x01', $startbb);
	$replace = false;

	$xpath = new DomXPath($doc);

	$list = $xpath->query("//".$oldnode);

	foreach ($list as $oldNode) {

		if ($oldnode == 'li')
			hz_syslog(print_r($oldNode,true));

		$attr = array();
		if ($oldNode->attributes->length)
			foreach ($oldNode->attributes as $attribute)
				$attr[$attribute->name] = $attribute->value;

		$replace = true;

		$startbb = $savestart;

		$i = 0;

		foreach ($attributes as $attribute => $value) {

			$startbb = str_replace('\x01'.++$i, '$1', $startbb);

			if (strpos('*'.$startbb, '$1') > 0) {

				if ($replace and (@$attr[$attribute] != '')) {

					$startbb = preg_replace($value, $startbb, $attr[$attribute], -1, $count);

					// If nothing could be changed
					if ($count == 0)
						$replace = false;
				} else
					$replace = false;
			} else {
				if (@$attr[$attribute] != $value)
					$replace = false;
			}
		}

		if ($replace) {
			$StartCode = $oldNode->ownerDocument->createTextNode($startbb);
			$EndCode = $oldNode->ownerDocument->createTextNode($endbb);

			$oldNode->parentNode->insertBefore($StartCode, $oldNode);

			if ($oldNode->hasChildNodes()) {
				foreach ($oldNode->childNodes as $child) {
					$newNode = $child->cloneNode(true);

					// Newlines are insignificant in HTML, but not so in BBCode, so let's
					// unwrap the child nodes of when converting them. Also we compress
					// consecutive whitespace chars to one.
					//
					// The exception is `<pre>` and `<code>` elements which
					// should keep both newlines and whitespace intact.
					if ($oldNode->nodeName != 'pre' && $oldNode->nodeName != 'code') {

						$newNode->nodeValue = str_replace(
							array("\n<", ">\n", "\r", "\n", "\xC3\x82\xC2\xA0"),
							array("<", ">", "<br />", " ", ""),
							htmlspecialchars($newNode->nodeValue, ENT_COMPAT, 'UTF-8', false));

						$newNode->nodeValue = preg_replace('=[\s]{2,}=i', " ", htmlspecialchars($newNode->nodeValue, ENT_COMPAT, 'UTF-8', false));
					}

					$oldNode->parentNode->insertBefore($newNode, $oldNode);
				}
			}

			$oldNode->parentNode->insertBefore($EndCode, $oldNode);
			$oldNode->parentNode->removeChild($oldNode);
		}
	}
	return($replace);
}

function deletenode(&$doc, $node)
{
	$xpath = new DomXPath($doc);
	$list = $xpath->query("//".$node);
	foreach ($list as $child)
		$child->parentNode->removeChild($child);
}

function html2bbcode($message)
{

	if(!is_string($message))
		return;

	$message = str_replace("\r", "", $message);


	// remove namespaces
	$message = preg_replace('=<(\w+):(.+?)>=', '<removeme>', $message);
	$message = preg_replace('=</(\w+):(.+?)>=', '</removeme>', $message);

	// mb_convert_encoding() is deprecated
	//$message = mb_convert_encoding($message, 'HTML-ENTITIES', "UTF-8");
	$message = mb_encode_numericentity($message, [0x80, 0x10FFFF, 0, ~0], 'UTF-8');

	// TODO: It would be better to do the list parsing with node2bbcode() but it has serious issues when
	// parsing nested lists. Especially if the li tag has no closing tag (which is valid).

	$message = preg_replace('/\<ul(.*?)\>/', '[list]', $message);
	$message = preg_replace('/\<ol(.*?)\>/', '[list=1]', $message);

	$message = str_replace(['<li><p>', '</p></li>'], ['<li>', '</li>'], $message);
	$message = preg_replace('/\<li(.*?)\>/', '[*]', $message);
	$message = str_replace(['</ul>', '</ol>'], '[/list]', $message);
	$message = str_replace('</li>', '', $message);

	if(!$message)
		return;

	$doc = new DOMDocument();
	$doc->preserveWhiteSpace = false;

	@$doc->loadHTML($message);

	deletenode($doc, 'style');
	deletenode($doc, 'head');
	deletenode($doc, 'title');
	deletenode($doc, 'meta');
	deletenode($doc, 'xml');
	deletenode($doc, 'removeme');

	node2bbcode($doc, 'html', array(), "", "");
	node2bbcode($doc, 'body', array(), "", "");

	// Outlook-Quote - Variant 1
	node2bbcode($doc, 'p', array('class'=>'MsoNormal', 'style'=>'margin-left:35.4pt'), '[quote]', '[/quote]');

	// Outlook-Quote - Variant 2
	node2bbcode($doc, 'div', array('style'=>'border:none;border-left:solid blue 1.5pt;padding:0cm 0cm 0cm 4.0pt'), '[quote]', '[/quote]');

	// MyBB-Stuff
	node2bbcode($doc, 'span', array('style'=>'text-decoration: underline;'), '[u]', '[/u]');
	node2bbcode($doc, 'span', array('style'=>'font-style: italic;'), '[i]', '[/i]');
	node2bbcode($doc, 'span', array('style'=>'font-weight: bold;'), '[b]', '[/b]');

	/*node2bbcode($doc, 'font', array('face'=>'/([\w ]+)/', 'size'=>'/(\d+)/', 'color'=>'/(.+)/'), '[font=$1][size=$2][color=$3]', '[/color][/size][/font]');
	node2bbcode($doc, 'font', array('size'=>'/(\d+)/', 'color'=>'/(.+)/'), '[size=$1][color=$2]', '[/color][/size]');
	node2bbcode($doc, 'font', array('face'=>'/([\w ]+)/', 'size'=>'/(.+)/'), '[font=$1][size=$2]', '[/size][/font]');
	node2bbcode($doc, 'font', array('face'=>'/([\w ]+)/', 'color'=>'/(.+)/'), '[font=$1][color=$3]', '[/color][/font]');
	node2bbcode($doc, 'font', array('face'=>'/([\w ]+)/'), '[font=$1]', '[/font]');
	node2bbcode($doc, 'font', array('size'=>'/(\d+)/'), '[size=$1]', '[/size]');
	node2bbcode($doc, 'font', array('color'=>'/(.+)/'), '[color=$1]', '[/color]');
*/
	// Untested
	//node2bbcode($doc, 'span', array('style'=>'/.*font-size:\s*(.+?)[,;].*font-family:\s*(.+?)[,;].*color:\s*(.+?)[,;].*/'), '[size=$1][font=$2][color=$3]', '[/color][/font][/size]');
	//node2bbcode($doc, 'span', array('style'=>'/.*font-size:\s*(\d+)[,;].*/'), '[size=$1]', '[/size]');
	//node2bbcode($doc, 'span', array('style'=>'/.*font-size:\s*(.+?)[,;].*/'), '[size=$1]', '[/size]');

	node2bbcode($doc, 'span', array('style'=>'/.*color:\s*(.+?)[,;].*/'), '[color="$1"]', '[/color]');
	//node2bbcode($doc, 'span', array('style'=>'/.*font-family:\s*(.+?)[,;].*/'), '[font=$1]', '[/font]');

	//node2bbcode($doc, 'div', array('style'=>'/.*font-family:\s*(.+?)[,;].*font-size:\s*(\d+?)pt.*/'), '[font=$1][size=$2]', '[/size][/font]');
	//node2bbcode($doc, 'div', array('style'=>'/.*font-family:\s*(.+?)[,;].*font-size:\s*(\d+?)px.*/'), '[font=$1][size=$2]', '[/size][/font]');
	//node2bbcode($doc, 'div', array('style'=>'/.*font-family:\s*(.+?)[,;].*/'), '[font=$1]', '[/font]');

	node2bbcode($doc, 'strong', array(), '[b]', '[/b]');
	node2bbcode($doc, 'em', array(), '[i]', '[/i]');
	node2bbcode($doc, 'b', array(), '[b]', '[/b]');
	node2bbcode($doc, 'i', array(), '[i]', '[/i]');
	node2bbcode($doc, 'u', array(), '[u]', '[/u]');
	// The s tag is deprecated in HTML5
	node2bbcode($doc, 's', array(), '[s]', '[/s]');
	node2bbcode($doc, 'del', [], '[s]', '[/s]');


	node2bbcode($doc, 'mark', array(), '[mark]', '[/mark]');
	node2bbcode($doc, 'span', array(), "", "");

	node2bbcode($doc, 'big', array(), "[size=large]", "[/size]");
	node2bbcode($doc, 'small', array(), "[size=small]", "[/size]");

	// Use a temporary tag to keep line breaks
	node2bbcode($doc, 'br', array(), '[br]', '');

	node2bbcode($doc, 'a', array('href'=>'/(.+)/'), '[url=$1]', '[/url]');

	node2bbcode($doc, 'img', array('src'=>'/(.+)/', 'width'=>'/(\d+)/', 'height'=>'/(\d+)/'), '[img=$2x$3]$1', '[/img]');
	node2bbcode($doc, 'img', array('src'=>'/(.+)/', 'alt'=>'/(.+)/'), '[img=$1]$2', '[/img]');
	node2bbcode($doc, 'img', array('src'=>'/(.+)/'), '[img]$1', '[/img]');

	node2bbcode($doc, 'video', array('src'=>'/(.+)/'), '[video]$1', '[/video]');
	node2bbcode($doc, 'audio', array('src'=>'/(.+)/'), '[audio]$1', '[/audio]');
//	node2bbcode($doc, 'iframe', array('src'=>'/(.+)/'), '[iframe]$1', '[/iframe]');

	node2bbcode($doc, 'hr', array(), "[hr]", "");

	node2bbcode($doc, 'th', array(), "[th]", "[/th]");
	node2bbcode($doc, 'tr', array(), "[tr]", "[/tr]");
	node2bbcode($doc, 'td', array(), "[td]", "[/td]");
	node2bbcode($doc, 'table', array(), "[table]", "[/table]");


	node2bbcode($doc, 'h1', array(), "[h1]", "[/h1]");
	node2bbcode($doc, 'h2', array(), "[h2]", "[/h2]");
	node2bbcode($doc, 'h3', array(), "[h3]", "[/h3]");
	node2bbcode($doc, 'h4', array(), "[h4]", "[/h4]");
	node2bbcode($doc, 'h5', array(), "[h5]", "[/h5]");
	node2bbcode($doc, 'h6', array(), "[h6]", "[/h6]");


	node2bbcode($doc, 'blockquote', array(), '[quote]', '[/quote]');
	node2bbcode($doc, 'pre', array(), "", "");

	node2bbcode($doc, 'code', array('class'=>'/(.+)/'), '[code=$1]', '[/code]');
	node2bbcode($doc, 'code', array(), '[code]', '[/code]');

	node2bbcode($doc, 'p', array('class'=>'MsoNormal'), "\n", "");
	node2bbcode($doc, 'p', array(), "\n", "\n");

	node2bbcode($doc, 'div', array('class'=>'MsoNormal'), "\r", "");
	node2bbcode($doc, 'div', array(), "\r", "\r");

	$message = $doc->saveHTML();

	// I'm removing the UTF-8 encoding of a NO-BREAK SPACE codepoint
	$message = str_replace(chr(194).chr(160), ' ', $message);

	$message = str_replace("&nbsp;", " ", $message);

	// removing multiple DIVs
	$message = preg_replace('=\r *\r=i', "\n", $message);
	$message = str_replace("\r", "\n", $message);

	call_hooks('html2bbcode', $message);

	$message = strip_tags($message);

	$message = html_entity_decode($message, ENT_QUOTES, 'UTF-8');

	$message = str_replace(array("<"), array("&lt;"), $message);

	// remove quotes if they don't make sense
	$message = preg_replace('=\[/quote\][\s]*\[quote\]=i', "\n", $message);

	$message = preg_replace('=\[quote\]\s*=i', "[quote]", $message);
	$message = preg_replace('=\s*\[/quote\]=i', "[/quote]", $message);

	do {
		$oldmessage = $message;
		$message = str_replace("\n \n", "\n\n", $message);
	} while ($oldmessage != $message);

	do {
		$oldmessage = $message;
		$message = str_replace("\n\n\n", "\n\n", $message);
	} while ($oldmessage != $message);

	do {
		$oldmessage = $message;
		$message = str_replace(array(
					"[/size]\n\n",
					"\n[br]",
					"[br]\n",
					"\n[hr]",
					"[hr]\n",
					"\n[list",
					"[/list]\n",
					"\n[/",
					"[list]\n",
					"[list=1]\n",
					"\n[*]"),
				array(
					"[/size]\n",
					"[br]",
					"[br]",
					"[hr]",
					"[hr]",
					"[list",
					"[/list]",
					"[/",
					"[list]",
					"[list=1]",
					"[*]"),
				$message);
	} while ($message != $oldmessage);

	$message = str_replace(array('[b][b]', '[/b][/b]', '[i][i]', '[/i][/i]'),
		array('[b]', '[/b]', '[i]', '[/i]'), $message);

	// Restore linebreaks from temp tag
	$message = preg_replace('/\[br\]\s?/', "\n", $message);

	// Handling Yahoo style of mails
	//	$message = str_replace('[hr][b]From:[/b]', '[quote][b]From:[/b]', $message);

	$message = htmlspecialchars($message,ENT_COMPAT,'UTF-8',false);
	return(trim($message));
}

