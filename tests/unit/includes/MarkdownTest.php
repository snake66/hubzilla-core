<?php
/*
 * Copyright (c) 2017 Hubzilla
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace Zotlabs\Tests\Unit\includes;

use Zotlabs\Tests\Unit\UnitTestCase;

require_once 'include/markdown.php';

/**
 * @brief Unit Test case for markdown functions.
 */
class MarkdownTest extends UnitTestCase {

	/**
	 * @dataProvider markdown_to_bbcode_provider
	 */
	public function test_markdown_to_bbcode(string $expected, string $src): void {
		$this->assertEquals($expected, markdown_to_bb($src));
	}

	public static function markdown_to_bbcode_provider(): array {
		return [
			'empty text' => [
				'',
				''
			],
			'plain text' => [
				'This is a test',
				'This is a test'
			],
			'bold and italic' => [
				'This is a test of [b]bold text[/b], [i]italic text[/i] and [b][i]bold and italic text[/i][/b]',
				'This is a test of **bold text**, *italic text* and ***bold and italic text***'
			],
			'multiline text' => [
				'This text is text wrapped over multiple lines.',
				"This text is\ntext wrapped\nover multiple\nlines."
			],
			'text with hard linebreak' => [
				"Line one\nLine two",
				"Line one  \nLine two"
			],
			'paragraphs' => [
				"Paragraph one\n\nParagraph two",
				"Paragraph one\n\nParagraph two",
			],
			'inline image' => [
				'[img=https://example.com/image.jpg]https://example.com/image.jpg[/img]',
				'![](https://example.com/image.jpg)'
			],
			'inline image with alt text' => [
				'[img=https://example.com/image.jpg]Alt text[/img]',
				'![Alt text](https://example.com/image.jpg)'
			],
			'inline code' => [
				'[code]some code[/code]',
				'`some code`'
			],
			'inline code with wrapped text' => [
				'[code]some code unwrapped[/code]',
				"`some code\n   unwrapped`"
			],
			'code block no language' => [
				"[code]some code\nover multiple lines[/code]",
				"```\nsome code\nover multiple lines\n```"
			],
			'code block no language indented' => [
				"[code]some code\n    over multiple lines\n    with indentation[/code]",
				"```\nsome code\n    over multiple lines\n    with indentation\n```"
			],
			'code block with language' => [
				"[code=php]&lt;?php\necho phpinfo();[/code]",
				"```php\n<?php\necho phpinfo();\n```"
			],
			'code block with URL' => [
				"[code]an example url https://example.com[/code]",
				"```\nan example url https://example.com\n```"
			],
			'bbcode code block with URL' => [
				"[code] proxy_pass http://example.com; [/code]",
				"[code]\nproxy_pass http://example.com;\n[/code]"
			]
		];
	}

	/**
	 * @covers ::html2markdown
	 * @dataProvider html2markdownProvider
	 */
	public function testHtml2markdown(string $html, string $markdown): void {
		$this->assertEquals($markdown, html2markdown($html));
	}

	public static function html2markdownProvider(): array {
		return [
				'empty text' => [
						'',
						''
				],
				'space and nbsp only' => [
						' &nbsp;',
						''
				],

				'strong, b, em, i, bib' => [
						'<strong>strong</strong> <b>bold</b> <em>em</em> <i>italic</i>  <b>bo<i>italic</i>ld</b>',
						'**strong** **bold** *em* *italic* **bo*italic*ld**'
				],

				'empty tags' => [
						'text1 <b></b> text2 <i></i>',
						'text1  text2'
				],
				'HTML entities' => [
						'& gt > lt <',
						'&amp; gt &gt; lt &lt;'
				],
				'escaped HTML entities' => [
						'&amp; lt &lt; gt &gt;',
						'&amp; lt &lt; gt &gt;'
				],
				'linebreak' => [
						"line1<br>line2\nline3",
						"line1  \nline2 line3"
				],
				'headlines' => [
						'<h1>header1</h1><h3>Header 3</h3>',
						"header1\n=======\n\n### Header 3"
				],
				'unordered list' => [
						'<ul><li>Item 1</li><li>Item 2</li><li>Item <b>3</b></li></ul>',
						"- Item 1\n- Item 2\n- Item **3**"
				],
				'ordered list' => [
						'<ol><li>Item 1</li><li>Item 2</li><li>Item <b>3</b></li></ol>',
						"1. Item 1\n2. Item 2\n3. Item **3**"
				],
				'nested lists' => [
						'<ul><li>Item A</li><li>Item B<ul><li>Nested A</li><li>Nested B</li></ul></li><li>Item C</li></ul>',
						"- Item A\n- Item B\n    - Nested A\n    - Nested B\n- Item C"
				],
				'img' => [
						'<img src="/path/to/img.png" alt="alt text" title="title text">',
						'![alt text](/path/to/img.png "title text")'
				],
				'link' => [
						'<a href="http://hubzilla.org" title="Hubzilla">link</a>',
						'[link](http://hubzilla.org "Hubzilla")'
				],
				'img link' => [
						'<a href="http://hubzilla.org" title="Hubzilla"><img src="/img/hubzilla.png" alt="alt img text" title="img title"></a>',
						'[![alt img text](/img/hubzilla.png "img title")](http://hubzilla.org "Hubzilla")'
				],
				'script' => [
						"<script>alert('test');</script>",
						"<script>alert('test');</script>"
				],
				'blockquote, issue #793' => [
						'<blockquote>something</blockquote>blah',
						"> something\n\nblah"
				],
				'code' => [
						'<code>&lt;p&gt;HTML text&lt;/p&gt;</code>',
						'`<p>HTML text</p>`'
				],
				'pre' => [
						'<pre>  one line with spaces  </pre>',
						"```\n  one line with spaces  \n```"
				],
				'div p' => [
						'<div>div</div><div><p>p</p></div>',
						"<div>div</div><div>p\n\n</div>"
				]
		];
	}

	public function test_bb_to_markdown(): void {
		$input = "test[b]bold[/b]\n[i]i[/i][ul][li]li1[/li][li]li2[/li][/ul]\n";
		$expected = "test**bold**  \n*i*\n\n- li1\n- li2";

		$this->assertEquals($expected, bb_to_markdown($input));
	}
}
